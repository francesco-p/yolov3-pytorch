from __future__ import division

from models import *
from utils.utils import *
from utils.datasets import *
from utils.parse_config import *

import cv2
import os
import sys
import time
import datetime
import argparse
import ipdb
import tqdm

import torch
from torch.utils.data import DataLoader
from torchvision import datasets
from torchvision import transforms
from torch.autograd import Variable
import torch.optim as optim
import visdom

# Removes warnings from the output
import warnings
warnings.filterwarnings('ignore')

def gen_train_validation_file(n_train, n_validation, imgs_folder):

    train_path = "data/txts/train.txt"
    with open(train_path, 'w') as f:
        for i in range(n_train):
            f.write(f"data/synth-shelves/{imgs_folder}/{i:05}.png\n")
    
    validation_path = "data/txts/validation.txt"
    with open(validation_path, 'w') as f:
        for i in range(n_train, n_train+n_validation):
            f.write(f"data/synth-shelves/{imgs_folder}/{i:05}.png\n")

    return train_path, validation_path 

######################################################
######################################################

# To use secondary gpu
#torch.cuda.set_device(1)

START_TIME = '2019_02_11_135025'

TRAIN_PATH, VALIDATION_PATH = gen_train_validation_file(7000, 3000, "imgs_random_hist_gauss")
#VALIDATION_PATH = "./data/txts/real_832_noted.txt"

OPTIM='adam'
MODEL_CONFIG_PATH = "./config/yolov3.cfg"
CHECKPOINT_DIR = f"./checkpoints/{OPTIM}/{START_TIME}"
if not os.path.exists(CHECKPOINT_DIR):
    sys.exit(f"[ERROR] {CHECKPOINT_DIR} not found.")

OUTPUT_DIR = f"./output/{START_TIME}"
#OUTPUT_DIR = "./output/real_832_noted"

if not os.path.exists(OUTPUT_DIR):
    os.mkdir(OUTPUT_DIR)
N_CPU = 4
IOU_THRES = 0.5
CONF_THRES = 0.99
NMS_THRES = 0.59
IMG_SIZE = 832
BATCH_SIZE = 4
CUDA = True
MAX_OBJECTS_TOBE_DETECTED = 100
NUM_CLASSES = 51
######################################################
######################################################

Tensor = torch.cuda.FloatTensor if CUDA else torch.FloatTensor

model = Darknet(MODEL_CONFIG_PATH)

max_map = -1
best_weights = "No best weights"

vis = visdom.Visdom()
first_plot = True

weights_list = sorted(os.listdir(CHECKPOINT_DIR))

start = time.time()

for weights in weights_list:

    if weights == "hyperparams.txt":
        continue

    epoch = int(weights[:4]) - 1
    
    print(f"[LOADING WEIGHTS] {weights}")

    # Initiate model
    model.load_weights(f"{CHECKPOINT_DIR}/{weights}")

    if CUDA:
        torch.cuda.set_device(1)
        model = model.cuda()

    model.eval()

    # Get dataloader
    validation_dataloader = torch.utils.data.DataLoader(ListDataset(VALIDATION_PATH, img_size=IMG_SIZE, max_objects=MAX_OBJECTS_TOBE_DETECTED), batch_size=BATCH_SIZE, shuffle=False, num_workers=N_CPU)

    all_detections = []
    all_annotations = []
    imidx=0

    for batch_i, (_, imgs, targets) in enumerate(validation_dataloader):

        print(f"Batch : {batch_i}/{len(validation_dataloader)}", end='\r')

        imgs_ori = np.asarray(imgs)
        imgs_t = Variable(imgs.type(Tensor))
        
        with torch.no_grad():
            outputs = model(imgs_t)
            outputs = non_max_suppression(outputs, NUM_CLASSES, conf_thres=CONF_THRES, nms_thres=NMS_THRES)

        for output, annotations, img in zip(outputs, targets, imgs_ori):
            im = np.ascontiguousarray(img[:,:,:].transpose([1,2,0])*255, dtype=np.uint8)
            
            all_detections.append([np.array([]) for _ in range(NUM_CLASSES)])
            if output is not None:
                
                # Get predicted boxes, confidence scores and labels
                pred_boxes = output[:, :5].cpu().numpy()
                scores = output[:, 4].cpu().numpy()
                pred_labels = output[:, -1].cpu().numpy()

                # Order by confidence
                sort_i = np.argsort(scores)
                pred_labels = pred_labels[sort_i]
                pred_boxes = pred_boxes[sort_i]

                for label in range(NUM_CLASSES):
                    all_detections[-1][label] = pred_boxes[pred_labels == label]
                    

                # Prediction boxes (Red)
                for box, label in zip(pred_boxes, pred_labels):
                    box = box.astype('int32')
                    cv2.rectangle(im, (box[0],box[1]), (box[2],box[3]), (255,0,0))
                    cv2.putText(im, f"{int(label)}", (box[0], box[1] +30), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 1, cv2.LINE_AA)
                
                
                imidx=imidx+1;
            else:
                print("None output")
                
            all_annotations.append([np.array([]) for _ in range(NUM_CLASSES)])
            if any(annotations[:, -1] > 0):

                annotation_labels = annotations[annotations[:, -1] > 0, 0].numpy()
                _annotation_boxes = annotations[annotations[:, -1] > 0, 1:]

                # Reformat to x1, y1, x2, y2 and rescale to image dimensions
                annotation_boxes = np.empty_like(_annotation_boxes)
                annotation_boxes[:, 0] = _annotation_boxes[:, 0] - _annotation_boxes[:, 2] / 2
                annotation_boxes[:, 1] = _annotation_boxes[:, 1] - _annotation_boxes[:, 3] / 2
                annotation_boxes[:, 2] = _annotation_boxes[:, 0] + _annotation_boxes[:, 2] / 2
                annotation_boxes[:, 3] = _annotation_boxes[:, 1] + _annotation_boxes[:, 3] / 2
                annotation_boxes *= IMG_SIZE
                
                # Ground truth boxes (Green)
                for box, label in zip(annotation_boxes, annotation_labels):
                    box = box.astype('int32')
                    cv2.rectangle(im, (box[0],box[1]), (box[2],box[3]), (0,255,0))
                    cv2.putText(im, f"{int(label)}", (box[0], box[1]), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 1, cv2.LINE_AA)
                
                for label in range(NUM_CLASSES):
                    all_annotations[-1][label] = annotation_boxes[annotation_labels == label, :]
            
        
        # Save just the last image            
        img_path = f"{OUTPUT_DIR}/{epoch:05}.png"
        #im = cv2.resize(im[:,:,[2,1,0]], (0,0), fx=2, fy=2) 
        cv2.imwrite(img_path, im[:,:,[2,1,0]])

    average_precisions = {}

    for label in range(NUM_CLASSES):
        true_positives = []
        scores = []
        num_annotations = 0

        for i in tqdm.tqdm(range(len(all_annotations)), desc=f"Computing AP for class '{label}'"):
            detections = all_detections[i][label]
            annotations = all_annotations[i][label]

            num_annotations += annotations.shape[0]
            detected_annotations = []

            for *bbox, score in detections:
                scores.append(score)

                if annotations.shape[0] == 0:
                    true_positives.append(0)
                    continue

                overlaps = bbox_iou_numpy(np.expand_dims(bbox, axis=0), annotations)
                assigned_annotation = np.argmax(overlaps, axis=1)
                max_overlap = overlaps[0, assigned_annotation]

                if max_overlap >= IOU_THRES and assigned_annotation not in detected_annotations:
                    true_positives.append(1)
                    detected_annotations.append(assigned_annotation)
                else:
                    true_positives.append(0)

        # no annotations -> AP for this class is 0
        if num_annotations == 0:
            average_precisions[label] = 0
            continue

        true_positives = np.array(true_positives)
        false_positives = np.ones_like(true_positives) - true_positives
        # sort by score
        indices = np.argsort(-np.array(scores))
        false_positives = false_positives[indices]
        true_positives = true_positives[indices]

        # compute false positives and true positives
        false_positives = np.cumsum(false_positives)
        true_positives = np.cumsum(true_positives)

        # compute recall and precision
        recall = true_positives / num_annotations
        precision = true_positives / np.maximum(true_positives + false_positives, np.finfo(np.float64).eps)

        # compute average precision
        average_precision = compute_ap(recall, precision)
        average_precisions[label] = average_precision

    elapsed = int((time.time() - start) // 60)

    print("Average Precisions:")
    ap_s = ""
    for c, ap in average_precisions.items():
        ap_s += f"{ap}," 
        print(f"+ Class '{c}' - AP: {ap}")

    mAP = np.mean(list(average_precisions.values()))
    with open(f"/tmp/validation_{START_TIME}.csv", 'a') as fp:
        fp.write(f"{epoch}," + ap_s + f"{mAP}\n")
    print(f"mAP: {mAP}")

    if mAP > max_map:
        max_map = mAP
        best_weights = weights
    print(f"Current best weights: {best_weights}\n\n")

    info = f"<h3>YOLO V3 test hyperparameters {VALIDATION_PATH}</h3> <b>Intersection Over Union Threshold: </b><code>{IOU_THRES}</code> <br /> <b>Confidence Threshold: </b><code>{CONF_THRES}</code> <br /> <b>Non-maximum suppression Threshold: </b><code>{NMS_THRES}</code> <br /> <b>Max object to be detected: </b><code>{MAX_OBJECTS_TOBE_DETECTED}</code> <br /> <b>Time elapsed (min): </b><code>{elapsed}</code> <br /> <b>Batch size: </b><code>{BATCH_SIZE}</code> <br /> <b>Image Size: </b><code>{IMG_SIZE}</code> <hr /> <b>Current : </b> <br /> <code>Weights: {weights} Epoch: {epoch}, mAP: {mAP}, Current best weights: {best_weights}</code>" 

    if first_plot:
        opts = dict(title=f'[Test] YOLO V3 {len(validation_dataloader)*BATCH_SIZE} Imgs {VALIDATION_PATH}', xlabel='Epoch', ylabel='Mean Avg. Precision')
        w0 = vis.text(info, opts=opts)
        w1 = vis.line(X=[epoch], Y=[mAP], opts=opts)
        first_plot = False
    else:
        vis.text(info, win=w0)
        vis.line(X=[epoch], Y=[mAP], win=w1, update='append', opts=opts)
    

    
