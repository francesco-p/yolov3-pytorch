"""
Coding: utf-8
Indentation: 4 spaces
"""
import numpy as np
import cv2
import os
from imgaug import augmenters as iaa
import random 
import sys

def draw_boxes(img, box, shape):
    shape = shape[0], shape[1]
    x1, y1, x2, y2 = from_yolo_to_cor(box, shape)
    cv2.rectangle(img, (x1, y1), (x2, y2), (0,255,0), 3)

def from_yolo_to_cor(box, shape):
    img_h, img_w = shape
    # x1, y1 = ((x + witdth)/2)*img_width, ((y + height)/2)*img_height
    # x2, y2 = ((x - witdth)/2)*img_width, ((y - height)/2)*img_height
    x1, y1 = int((box[0] + box[2]/2)*img_w), int((box[1] + box[3]/2)*img_h)
    x2, y2 = int((box[0] - box[2]/2)*img_w), int((box[1] - box[3]/2)*img_h)
    return x1, y1, x2, y2

name = sys.argv[1][:-4]

img = cv2.imread(f'{name}.png')
# Check if bounding boxes are correct
with open(f"{name}.txt", 'r') as f:
    for line in f.readlines():
        box = tuple(float(e) for e in line.split(' ')[1:])
        draw_boxes(img, box, img.shape)


cv2.imshow("img", cv2.resize(img, (1024,1024)))
if cv2.waitKey() == 27:
    cv2.destroyAllWindows()
