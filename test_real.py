from __future__ import division

from models import *
from utils.utils import *
from utils.datasets import *
from utils.parse_config import *

import cv2
import os
import sys
import time
import datetime
import argparse
import tqdm

import torch
from torch.utils.data import DataLoader
from torchvision import datasets
from torchvision import transforms
from torch.autograd import Variable
import torch.optim as optim
from PIL import Image
from resizeimage import resizeimage
from shutil import copyfile
import os
import matplotlib.pyplot as plt

NUM_CLASSES = 51
CONF_THRES = 0.90
NMS_THRES = 0.50
CLASS_PATH = './data/txts/synth_real.names'
START_TIME = '2019_02_11_135025'
BEST_WEIGHTS = '0300.weights'
OUTPUT_DIR = f"./output/real/{START_TIME}"
if not os.path.exists(OUTPUT_DIR):
    os.mkdir(OUTPUT_DIR)

classes = load_classes_names(CLASS_PATH)

# Initiate model
model = Darknet('config/yolov3.cfg')
model.load_weights(f'checkpoints/adam/{START_TIME}/{BEST_WEIGHTS}')

img_size = 832
files = sorted(os.listdir('./data/real_832_noted/'))
files = [x for x in files if x[-3:]=='png']
for i,f in enumerate(files):
    f=f'./data/real_832_noted/{f}'
    image = Image.open(f)
    img = np.asarray(resizeimage.resize_width(image, img_size)).transpose([2,0,1])
    img = np.expand_dims(img,0)

    for img  in torch.utils.data.DataLoader(img):
        im = img[0,:,:,:].cpu().numpy().transpose([1,2,0]).copy()
        
        outputs = model(img.float())
        outputs = non_max_suppression(outputs, NUM_CLASSES, conf_thres=CONF_THRES, nms_thres=NMS_THRES)

        for output in outputs:
            if output is not None:
                # Get predicted boxes, confidence scores and labels
                pred_boxes = output[:, :5].cpu().numpy()
                scores = output[:, 4].cpu().numpy()
                pred_labels = output[:, -1].cpu().numpy()

                # Order by confidence
                sort_i = np.argsort(scores)
                pred_labels = pred_labels[sort_i]
                pred_boxes = pred_boxes[sort_i]
 
                # Prediction boxes (Red)
                for box, label in zip(pred_boxes, pred_labels):
                    box = box.astype('int32')
                    cv2.rectangle(im, (box[0],box[1]), (box[2],box[3]), (255,0,0))
                    label = classes[int(label)]
                    cv2.putText(im, f"{label}", (box[0], box[1]), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 1, cv2.LINE_AA)

                cv2.imwrite(f"{OUTPUT_DIR}/0_{i:05}.png",im[:,:,[2,1,0]])
                #plt.imshow(im)
                #plt.savefig(f"output/real/0_{i:05}.png",im)
                print(f"processed {f} - {img.shape}")
            else:
                print(f"No detection for {f}")
