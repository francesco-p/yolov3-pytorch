from __future__ import division

from models import *
from utils.utils import *
from utils.datasets import *
from utils.parse_config import *

import os
import time
import datetime
import sys

import torch
import torch.nn as nn
from torch.utils.data import DataLoader
from torchvision import datasets
from torchvision import transforms
from torch.autograd import Variable
import torch.optim as optim
import logging
import ipdb
import visdom
import matplotlib.pyplot as plt
import cv2
import tqdm


# Removes warnings from the output
import warnings
warnings.filterwarnings('ignore')




######################################################
#                   MAIN CODE                        #
######################################################


OPTIM = 'adam'
IMG_SIZE = 832
EPOCHS = 200
BATCH_SIZE = 4
LEARNING_RATE = 0.00005
MAX_OBJECTS_TOBE_DETECTED = 80
MODEL_CONFIG_PATH = "./config/yolov3.cfg"
WEIGHTS_PATH = "./weights/yolov3.weights"
TRAIN_PATH = "data/synth-shelves/train.txt"
N_BATCH_TO_PLOT=10
N_CPU = 4
CUDA = True
DECAY = 0

NUM_CLASSES = 51

START_TIME = time.strftime("%Y_%m_%d_%H%M%S")
CHECKPOINT_DIR = f"./checkpoints/{OPTIM}/{START_TIME}"

OUTPUT_DIR = f"./output/{START_TIME}"
if not os.path.exists(OUTPUT_DIR):
    os.mkdir(OUTPUT_DIR)

IOU_THRES = 0.5
CONF_THRES = 0.80
NMS_THRES = 0.45
MAX_OBJECTS_TOBE_DETECTED = 100
VALIDATION_PATH = 'data/synth-shelves/validation.txt'

if not os.path.exists(CHECKPOINT_DIR):
    os.mkdir(CHECKPOINT_DIR)

max_map = -1
best_weights = "No best weights"

# Model definition
model = Darknet(MODEL_CONFIG_PATH, IMG_SIZE)
#model = nn.DataParallel(Darknet(MODEL_CONFIG_PATH, IMG_SIZE))

if WEIGHTS_PATH != "":
    model.load_weights(WEIGHTS_PATH)
    print(f"[WEIGHTS INIT] {WEIGHTS_PATH}")
else:
    model.apply(weights_init_normal)
    print(f"[WEIGHTS INIT] Random")

if CUDA:
    print(f"[CUDA] {CUDA}")
    model = model.cuda()

model.train()

# Get dataloaders
training_dataloader = torch.utils.data.DataLoader(ListDataset(TRAIN_PATH, IMG_SIZE, MAX_OBJECTS_TOBE_DETECTED), batch_size=BATCH_SIZE, shuffle=True, num_workers=N_CPU)

Tensor = torch.cuda.FloatTensor if CUDA else torch.FloatTensor

if OPTIM == 'sgd':
    optimizer = torch.optim.SGD(filter(lambda p: p.requires_grad, model.parameters()), lr=LEARNING_RATE, weight_decay=DECAY)
elif OPTIM == 'adam':
    optimizer = torch.optim.Adam(filter(lambda p: p.requires_grad, model.parameters()), lr=LEARNING_RATE, weight_decay=DECAY)
else:
    print("[ERROR] Select an optimizator.")
    sys.exit(0)
print(f"[OPTIMIZATION] {OPTIM}")

# Live Plot
vis = visdom.Visdom()
start = time.time()
first_test_plot = True
first_train_plot = True
first_img_plot = True
total_loss_y = []
total_loss_y_epoch = []
precision_y = []
precision_y_epoch = []

print("[INFO] Start Training")


for epoch in range(EPOCHS):

    for batch_i, (_, imgs, targets) in enumerate(training_dataloader):
        
        # Pre noise+blur
        #pre = imgs[0,:,:,:].cpu().numpy().transpose([1,2,0]).copy()[:,:,[2,1,0]]*255
        #cv2.imwrite("/tmp/pre.png", pre)

        #imgs += torch.randn(imgs.shape) * 0.08
        #post = cv2.GaussianBlur(imgs[0,:,:,:].numpy().transpose([1,2,0]), (5,5), 0.7, 0.7)

        # Post noise+blur
        #cv2.imwrite("/tmp/post.png", post[:,:,[2,1,0]]*255)

        imgs = Variable(imgs.type(Tensor))
        targets = Variable(targets.type(Tensor), requires_grad=False)
        optimizer.zero_grad()
        loss = model(imgs, targets)
        loss.backward()
        optimizer.step()

        loss_x = model.losses["x"],
        loss_y = model.losses["y"],
        loss_w = model.losses["w"],
        loss_h = model.losses["h"],
        conf = model.losses["conf"],
        cls = model.losses["cls"],
        total_loss = loss.item(),
        recall = model.losses["recall"],
        precision = model.losses["precision"],

        msg = f"[Epoch {epoch}/{EPOCHS}, Batch {batch_i}/{len(training_dataloader)}] [Losses: x {loss_x[0]:.5f}, y {loss_y[0]:.5f}, w {loss_w[0]:.5f}, h {loss_h[0]:.5f}, conf {conf[0]:.5f}, cls {cls[0]:.5f}, total {total_loss[0]:.5f}, recall: {recall[0]:.5f}, precision: {precision[0]:.5f}]"

        print(msg)
        
        model.seen += imgs.size(0)

        elapsed = int((time.time() - start) // 60)
        total_loss_y.append(total_loss)
        total_loss_y_epoch.append(total_loss)
        precision_y.append(precision)
        precision_y_epoch.append(precision)

        ## PLOT
        info = f"<h3>YOLO V3 training hyperparameters {START_TIME}</h3> <b>Optimizator:</b><code>{OPTIM}</code> <br /> <b>Time elapsed (min):</b><code>{elapsed}</code><br /> <b>Learning Rate:</b><code>{LEARNING_RATE}</code> <br /> <b>Decay:</b><code>{DECAY}</code> <br /> <b>Batch size:</b><code>{BATCH_SIZE}</code> <hr /> <b>Current batch:</b> <br /> <code>{msg}</code>" 

        if first_train_plot:
            w0 = vis.text(info)
        else:
            vis.text(info, win=w0)

        if batch_i % N_BATCH_TO_PLOT == 0:

            loss_opts = dict(title=f'[Training] YOLO V3 Loss (Log-scale) {START_TIME}', xlabel='Epoch', ylabel='Avg. Total Loss', ytype='log')
            precision_opts=dict(title=f'[Training] YOLO V3 Precision {START_TIME}', xlabel='Epoch', ylabel='Avg. Precision')

            if first_train_plot:

                w1 = vis.line(X=[epoch], Y=[np.mean(total_loss_y)], name=f'{N_BATCH_TO_PLOT}batches', opts=loss_opts)
                w2 = vis.line(X=[epoch], Y=[np.mean(precision_y)], name=f'{N_BATCH_TO_PLOT}batches', opts=precision_opts)

                vis.line(X=[epoch], Y=[np.mean(total_loss_y_epoch)], name='epoch', win=w1, update='append', opts=loss_opts)
                vis.line(X=[epoch], Y=[np.mean(precision_y_epoch)], name='epoch', win=w2, update='append', opts=precision_opts)

                first_train_plot = False

            else:
                vis.line(X=[epoch+(batch_i/len(training_dataloader))], Y=[np.mean(total_loss_y)], name=f'{N_BATCH_TO_PLOT}batches', win=w1, update='append', opts=loss_opts)
                vis.line(X=[epoch+(batch_i/len(training_dataloader))], Y=[np.mean(precision_y)], name=f'{N_BATCH_TO_PLOT}batches', win=w2, update='append', opts=precision_opts)

            total_loss_y = []
            precision_y = []

    # SAVE MODEL
    print(f"[MODEL SAVE] Savefile: {CHECKPOINT_DIR}/{(epoch+1):04d}.weights")
    model.save_weights(f"{CHECKPOINT_DIR}/{(epoch+1):04d}.weights")
   
    # Epoch plot
    vis.line(X=[epoch+1], Y=[np.mean(total_loss_y_epoch)], name='epoch', win=w1, update='append', opts=loss_opts)
    vis.line(X=[epoch+1], Y=[np.mean(precision_y_epoch)], name='epoch', win=w2, update='append', opts=precision_opts)
    total_loss_y_epoch = []
    precision_y_epoch = []






    ##########################################################################################################
    ##################################################### TEST ###############################################
    ##########################################################################################################




    Tensor = torch.cuda.FloatTensor if CUDA else torch.FloatTensor

    # Get dataloader
    validation_dataloader = torch.utils.data.DataLoader(ListDataset(VALIDATION_PATH, img_size=IMG_SIZE, max_objects=MAX_OBJECTS_TOBE_DETECTED), batch_size=BATCH_SIZE, shuffle=False, num_workers=N_CPU)

    all_detections = []
    all_annotations = []
    imidx=0
    tpi = transforms.ToPILImage()
    for batch_i, (_, imgs, targets) in enumerate(tqdm.tqdm(validation_dataloader, desc="Detecting objects")):


        imgs_ori = np.asarray(imgs)
        imgs_t = Variable(imgs.type(Tensor))
        

        with torch.no_grad():
            outputs = model(imgs_t)
            outputs = non_max_suppression(outputs, NUM_CLASSES, conf_thres=CONF_THRES, nms_thres=NMS_THRES)

        for output, annotations, img in zip(outputs, targets, imgs_ori):

            im = np.ascontiguousarray(img[:,:,:].transpose([1,2,0])*255, dtype=np.uint8)
            
            
            all_detections.append([np.array([]) for _ in range(NUM_CLASSES)])
            if output is not None:
                
                # Get predicted boxes, confidence scores and labels
                pred_boxes = output[:, :5].cpu().numpy()
                scores = output[:, 4].cpu().numpy()
                pred_labels = output[:, -1].cpu().numpy()

                # Order by confidence
                sort_i = np.argsort(scores)
                pred_labels = pred_labels[sort_i]
                pred_boxes = pred_boxes[sort_i]

                for label in range(NUM_CLASSES):
                    all_detections[-1][label] = pred_boxes[pred_labels == label]
                    

                # Prediction boxes (Red)
                for box, label in zip(pred_boxes, pred_labels):
                    box = box.astype('int32')
                    cv2.rectangle(im, (box[0],box[1]), (box[2],box[3]), (255,0,0))
                    cv2.putText(im, f"{int(label)}", (box[0], box[1] +30), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 1, cv2.LINE_AA)
                
                
                imidx=imidx+1;
            else:
                print("None output")
                
            all_annotations.append([np.array([]) for _ in range(NUM_CLASSES)])
            if any(annotations[:, -1] > 0):

                annotation_labels = annotations[annotations[:, -1] > 0, 0].numpy()
                _annotation_boxes = annotations[annotations[:, -1] > 0, 1:]

                # Reformat to x1, y1, x2, y2 and rescale to image dimensions
                annotation_boxes = np.empty_like(_annotation_boxes)
                annotation_boxes[:, 0] = _annotation_boxes[:, 0] - _annotation_boxes[:, 2] / 2
                annotation_boxes[:, 1] = _annotation_boxes[:, 1] - _annotation_boxes[:, 3] / 2
                annotation_boxes[:, 2] = _annotation_boxes[:, 0] + _annotation_boxes[:, 2] / 2
                annotation_boxes[:, 3] = _annotation_boxes[:, 1] + _annotation_boxes[:, 3] / 2
                annotation_boxes *= IMG_SIZE
                
                # Ground truth boxes (Green)
                for box, label in zip(annotation_boxes, annotation_labels):
                    box = box.astype('int32')
                    cv2.rectangle(im, (box[0],box[1]), (box[2],box[3]), (0,255,0))
                    cv2.putText(im, f"{int(label)}", (box[0], box[1]), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 1, cv2.LINE_AA)
                
                for label in range(NUM_CLASSES):
                    all_annotations[-1][label] = annotation_boxes[annotation_labels == label, :]
            
        
        # Save just the last image            
        im = cv2.resize(im[:,:,[2,1,0]], (0,0), fx=2, fy=2) 
        img_path = f"{OUTPUT_DIR}/epoch_{epoch:05}.png"
        cv2.imwrite(img_path,im)

        #vis.image((img*255).astype(np.uint8), opts=dict(title='titleee', caption='captionnn'))
        #tmp = imgs[0,:,:,:]*255


        #import torchvision
        #tmp = torchvision.utils.make_grid(imgs*255)
        #vis.image(tmp, opts=dict(title='titleee', caption='captionnn'))

        #vis.image((im*255).astype(np.uint8), opts=dict(title='titleee', caption='captionnn'))

    #plt.imshow(im[:,:,[2,1,0]])

    if first_img_plot:
        plt.figure(figsize=(10,10))
        plt.imshow(im[:,:,[2,1,0]])
        w5 = vis.matplot(plt)
        first_img_plot = False
    else:
        plt.figure(figsize=(10,10))
        plt.imshow(im[:,:,[2,1,0]])
        vis.matplot(plt, win=w5)

    average_precisions = {}

    for label in range(NUM_CLASSES):
        true_positives = []
        scores = []
        num_annotations = 0

        for i in tqdm.tqdm(range(len(all_annotations)), desc=f"Computing AP for class '{label}'"):
            detections = all_detections[i][label]
            annotations = all_annotations[i][label]

            num_annotations += annotations.shape[0]
            detected_annotations = []

            for *bbox, score in detections:
                scores.append(score)

                if annotations.shape[0] == 0:
                    true_positives.append(0)
                    continue

                overlaps = bbox_iou_numpy(np.expand_dims(bbox, axis=0), annotations)
                assigned_annotation = np.argmax(overlaps, axis=1)
                max_overlap = overlaps[0, assigned_annotation]

                if max_overlap >= IOU_THRES and assigned_annotation not in detected_annotations:
                    true_positives.append(1)
                    detected_annotations.append(assigned_annotation)
                else:
                    true_positives.append(0)

        # no annotations -> AP for this class is 0
        if num_annotations == 0:
            average_precisions[label] = 0
            continue

        true_positives = np.array(true_positives)
        false_positives = np.ones_like(true_positives) - true_positives
        # sort by score
        indices = np.argsort(-np.array(scores))
        false_positives = false_positives[indices]
        true_positives = true_positives[indices]

        # compute false positives and true positives
        false_positives = np.cumsum(false_positives)
        true_positives = np.cumsum(true_positives)

        # compute recall and precision
        recall = true_positives / num_annotations
        precision = true_positives / np.maximum(true_positives + false_positives, np.finfo(np.float64).eps)

        # compute average precision
        average_precision = compute_ap(recall, precision)
        average_precisions[label] = average_precision

    elapsed = int((time.time() - start) // 60)

    print("Average Precisions:")
    for c, ap in average_precisions.items():
        print(f"+ Class '{c}' - AP: {ap}")

    mAP = np.mean(list(average_precisions.values()))
    print(f"mAP: {mAP}")

    if mAP > max_map:
        max_map = mAP
        best_weights = epoch
    print(f"Current best epoch: {best_weights}\n\n")

    info = f"<h3>YOLO V3 test hyperparameters {START_TIME}</h3> <b>Intersection Over Union Threshold: </b><code>{IOU_THRES}</code> <br /> <b>Confidence Threshold: </b><code>{CONF_THRES}</code> <br /> <b>Non-maximum suppression Threshold: </b><code>{NMS_THRES}</code> <br /> <b>Max object to be detected: </b><code>{MAX_OBJECTS_TOBE_DETECTED}</code> <br /> <b>Time elapsed (min): </b><code>{elapsed}</code> <br /> <b>Batch size: </b><code>{BATCH_SIZE}</code> <br /> <b>Image Size: </b><code>{IMG_SIZE}</code> <hr /> <b>Current : </b> <br /> <code>Curr Epoch: {epoch} Epoch: {epoch}, mAP: {mAP}, Current best weights: {best_weights}</code>" 

    if first_test_plot:
        opts = dict(title=f'[Test] YOLO V3 {len(validation_dataloader)*BATCH_SIZE} Imgs {START_TIME}', xlabel='Epoch', ylabel='Mean Avg. Precision')
        w3 = vis.text(info, opts=opts)
        w4 = vis.line(X=[epoch+1], Y=[mAP], opts=opts)
        first_test_plot = False
    else:
        vis.text(info, win=w3)
        vis.line(X=[epoch+1], Y=[mAP], win=w4, update='append', opts=opts)
    

    

