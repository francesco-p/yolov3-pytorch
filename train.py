from __future__ import division

from models import *
from utils.utils import *
from utils.datasets import *
from utils.parse_config import *

import os
import time
import datetime
import sys

import torch
import torch.nn as nn
from torch.utils.data import DataLoader
from torchvision import datasets
from torchvision import transforms
from torch.autograd import Variable
import torch.optim as optim
import logging
import ipdb
import visdom
import matplotlib.pyplot as plt
import cv2
import fileinput

# Removes warnings from the output
import warnings
warnings.filterwarnings('ignore')


def gen_train_validation_file(n_train, n_validation, imgs_folder):

    train_path = "data/txts/train.txt"
    with open(train_path, 'w') as f:
        for i in range(n_train):
            f.write(f"data/synth-shelves/{imgs_folder}/{i:05}.png\n")
    
    validation_path = "data/txts/validation.txt"
    with open(validation_path, 'w') as f:
        for i in range(n_train, n_train+n_validation):
            f.write(f"data/synth-shelves/{imgs_folder}/{i:05}.png\n")

    return train_path, validation_path 

######################################################
######################################################

TRAIN_PATH, VALIDATION_PATH = gen_train_validation_file(7000, 3000, "imgs_hist_gauss")

OPTIM = 'adam'
IMG_SIZE = 832
START_EPOCHS = 0 
END_EPOCHS = 300
BATCH_SIZE = 4
LEARNING_RATE = 0.0001
MAX_OBJECTS_TOBE_DETECTED = 100
MODEL_CONFIG_PATH = "./config/yolov3.cfg"

START_TIME = time.strftime("%Y-%m-%d_%H%M%S")

WEIGHTS_PATH = "./weights/yolov3.weights"
#WEIGHTS_PATH = f"./checkpoints/{OPTIM}/{START_TIME}/0115.weights"
print(WEIGHTS_PATH)

N_CPU = 4
CUDA = True
DECAY = 0
N_BATCH_TO_PLOT=50

CHECKPOINT_DIR = f"./checkpoints/{OPTIM}/{START_TIME}"
if not os.path.exists(CHECKPOINT_DIR):
    os.mkdir(CHECKPOINT_DIR)

with open(f"{CHECKPOINT_DIR}/hyperparams.txt", "w") as f:

    s = f"OPTIM = {OPTIM}\nIMG_SIZE = {IMG_SIZE}\nSTART_EPOCHS = {START_EPOCHS}\nEND_EPOCHS = {END_EPOCHS}\nBATCH_SIZE = {BATCH_SIZE}\nLEARNING_RATE = {LEARNING_RATE}\nTRAIN_PATH, {TRAIN_PATH}\nMAX_OBJECTS_TOBE_DETECTED = {MAX_OBJECTS_TOBE_DETECTED}\nMODEL_CONFIG_PATH = {MODEL_CONFIG_PATH}\nWEIGHTS_PATH = {WEIGHTS_PATH}\nSTART_TIME = {START_TIME}\nCHECKPOINT_DIR = {CHECKPOINT_DIR}\nN_CPU = {N_CPU}\nCUDA = {CUDA}\nDECAY = {DECAY}\nN_BATCH_TO_PLOT={N_BATCH_TO_PLOT}\n"
    f.write(s)

with open(f"./csv/{START_TIME}.csv", 'a') as fp:
    fp.write(f"epoch,batch_i,loss_x,loss_y,loss_w,loss_h,conf,cls,total_loss,recall,precision\n")
######################################################
######################################################

# Model definition
model = Darknet(MODEL_CONFIG_PATH, IMG_SIZE)
#model = nn.DataParallel(Darknet(MODEL_CONFIG_PATH, IMG_SIZE))

if WEIGHTS_PATH != "":
    model.load_weights(WEIGHTS_PATH)
else:
    model.apply(weights_init_normal)

if CUDA:
    model = model.cuda()

model.train()

# Get dataloaders
training_dataloader = torch.utils.data.DataLoader(ListDataset(TRAIN_PATH, IMG_SIZE, MAX_OBJECTS_TOBE_DETECTED), batch_size=BATCH_SIZE, shuffle=True, num_workers=N_CPU)

Tensor = torch.cuda.FloatTensor if CUDA else torch.FloatTensor

if OPTIM == 'sgd':
    optimizer = torch.optim.SGD(filter(lambda p: p.requires_grad, model.parameters()), lr=LEARNING_RATE, weight_decay=DECAY)
elif OPTIM == 'adam':
    optimizer = torch.optim.Adam(filter(lambda p: p.requires_grad, model.parameters()), lr=LEARNING_RATE, weight_decay=DECAY)
else:
    print("[ERROR] Select an optimizator.")
    sys.exit(0)

# Live Plot
vis = visdom.Visdom()
start = time.time()
first_plot = True
total_loss_y = []
total_loss_y_epoch = []
precision_y = []
precision_y_epoch = []

for epoch in range(START_EPOCHS, END_EPOCHS):

    for batch_i, (_, imgs, targets) in enumerate(training_dataloader):

        # Pre noise+blur
        #pre = imgs[0,:,:,:].cpu().numpy().transpose([1,2,0]).copy()[:,:,[2,1,0]]*255
        #cv2.imwrite("/tmp/pre.png", pre)

        #imgs += torch.randn(imgs.shape) * 0.08
        #post = cv2.GaussianBlur(imgs[0,:,:,:].numpy().transpose([1,2,0]), (5,5), 0.7, 0.7)

        # Post noise+blur
        #cv2.imwrite("/tmp/post.png", post[:,:,[2,1,0]]*255)

        imgs = Variable(imgs.type(Tensor))
        targets = Variable(targets.type(Tensor), requires_grad=False)
        optimizer.zero_grad()
        loss = model(imgs, targets)
        loss.backward()
        optimizer.step()

        loss_x = model.losses["x"],
        loss_y = model.losses["y"],
        loss_w = model.losses["w"],
        loss_h = model.losses["h"],
        conf = model.losses["conf"],
        cls = model.losses["cls"],
        total_loss = loss.item(),
        recall = model.losses["recall"],
        precision = model.losses["precision"],

        msg = f"[Epoch {epoch}/{END_EPOCHS}, Batch {batch_i}/{len(training_dataloader)}] [Losses: x {loss_x[0]:.5f}, y {loss_y[0]:.5f}, w {loss_w[0]:.5f}, h {loss_h[0]:.5f}, conf {conf[0]:.5f}, cls {cls[0]:.5f}, total {total_loss[0]:.5f}, recall: {recall[0]:.5f}, precision: {precision[0]:.5f}]"

        print(msg)


        with open(f"./csv/{START_TIME}.csv", 'a') as fp:
            fp.write(f"{epoch},{batch_i},{loss_x[0]:.5f},{loss_y[0]:.5f},{loss_w[0]:.5f},{loss_h[0]:.5f},{conf[0]:.5f},{cls[0]:.5f},{total_loss[0]:.5f},{recall[0]:.5f},{precision[0]:.5f}\n")

        model.seen += imgs.size(0)

        elapsed = int((time.time() - start) // 60)
        total_loss_y.append(total_loss)
        total_loss_y_epoch.append(total_loss)
        precision_y.append(precision)
        precision_y_epoch.append(precision)


        ####################################################################
        ###### PLOT
        ####################################################################

        info = f"<h3>YOLO V3 training hyperparameters {START_TIME}</h3> <b>Optimizator:</b><code>{OPTIM}</code> <br /> <b>Time elapsed (min):</b><code>{elapsed}</code><br /> <b>Learning Rate:</b><code>{LEARNING_RATE}</code> <br /> <b>Decay:</b><code>{DECAY}</code> <br /> <b>Batch size:</b><code>{BATCH_SIZE}</code> <hr /> <b>Current batch:</b> <br /> <code>{msg}</code>"


        if first_plot:
            w0 = vis.text(info)
        else:
            vis.text(info, win=w0)

        if batch_i % N_BATCH_TO_PLOT == 0:

            loss_opts = dict(title=f'[Training] YOLO V3 Loss (Log-scale) {START_TIME}', xlabel='Epoch', ylabel='Avg. Total Loss', ytype='log')
            precision_opts=dict(title=f'[Training] YOLO V3 Precision {START_TIME}', xlabel='Epoch', ylabel='Avg. Precision')

            if first_plot:

                w1 = vis.line(X=[epoch], Y=[np.mean(total_loss_y)], name=f'{N_BATCH_TO_PLOT}batches', opts=loss_opts)
                w2 = vis.line(X=[epoch], Y=[np.mean(precision_y)], name=f'{N_BATCH_TO_PLOT}batches', opts=precision_opts)

                vis.line(X=[epoch], Y=[np.mean(total_loss_y_epoch)], name='epoch', win=w1, update='append', opts=loss_opts)
                vis.line(X=[epoch], Y=[np.mean(precision_y_epoch)], name='epoch', win=w2, update='append', opts=precision_opts)

                first_plot = False
            else:
                vis.line(X=[epoch+(batch_i/len(training_dataloader))], Y=[np.mean(total_loss_y)], name=f'{N_BATCH_TO_PLOT}batches', win=w1, update='append', opts=loss_opts)
                vis.line(X=[epoch+(batch_i/len(training_dataloader))], Y=[np.mean(precision_y)], name=f'{N_BATCH_TO_PLOT}batches', win=w2, update='append', opts=precision_opts)

            total_loss_y = []
            precision_y = []

    ####################################################################
    ###### SAVE MODEL
    ####################################################################
    print(f"[MODEL SAVE] Savefile: {CHECKPOINT_DIR}/{(epoch+1):04d}.weights")
    model.save_weights(f"{CHECKPOINT_DIR}/{(epoch+1):04d}.weights")

    ###### Epoch plot
    vis.line(X=[epoch+1], Y=[np.mean(total_loss_y_epoch)], name='epoch', win=w1, update='append', opts=loss_opts)
    vis.line(X=[epoch+1], Y=[np.mean(precision_y_epoch)], name='epoch', win=w2, update='append', opts=precision_opts)
    total_loss_y_epoch = []
    precision_y_epoch = []

